<?php

class FixedNodeRevisionLock {

  protected $__locked = false;
  protected $changed;
  protected $timestamp;
  protected $vid;

  public function __construct($object) {
    foreach((array) $object as $field => $value) {
      $this->$field = $value;
    }
    $this->lock();
  }

  public function lock() {
    $this->__locked = true;
  }

  public function unlock() {
    $this->__locked = false;
  }

  public function getObject() {
    $data = array();
    foreach((array) $this as $name => $value) {
      $data[$name] = $value;
    }
    return (object) ($data);
  }

  public function __get($name) {
    return $this->$name;
  }

  public function __isset($name) {
    return isset($this->$name);
  }

  public function __set($name, $value) {
    if(!$this->__locked) {
      $this->$name = $value;
    }
  }

  public function __unset($name) {
    if(!$this->__locked) {
      unset($this->$name);
    }
  }
}
